﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace KeyMacroRecorder
{
  public partial class MainForm : Form
  {
    private INPUT[] _myInputs;

    public MainForm()
    {
      InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      _myInputs = new INPUT[1];
      _myInputs[0].type = (uint)INPUT_Type.INPUT_KEYBOARD;
      _myInputs[0].U = new InputUnion();
    }

    private void StartButton_Click(object sender, EventArgs e)
    {
      StartButton.Enabled = false;

      // Parse the moves.
      List<Command> commands = new List<Command>();
      try
      {
        Parser.Parse(this.MovesTextBox.Text, commands);

        if (FocusTargetWindow())
        {
          // Run the scripted moves.
          foreach (var item in commands)
          {
            if (item.VirtualKeyShort.HasValue)
              Send(item.VirtualKeyShort.Value, item.Duration);
            else
              Thread.Sleep(item.Duration);
          }
        }
      }
      finally
      {
        commands = null;
      }

      StartButton.Enabled = true;
    }

    private bool FocusTargetWindow()
    {
      IntPtr hWnd = ExternMethods.FindWindowByCaption(IntPtr.Zero, ApplicationNameTextBox.Text);
      return ExternMethods.SetForegroundWindow(hWnd);
    }

    private void Send(VirtualKeyShort vks, int duration)
    {
      var scanCode = (ScanCodeShort)ExternMethods.MapVirtualKey((uint)vks, (uint)MAPVK.VK_TO_VSC);
      _myInputs[0].U.ki.dwFlags = IsExtendedKey(scanCode) ? KEYEVENTF.EXTENDEDKEY : 0;

      _myInputs[0].type = (uint)INPUT_Type.INPUT_KEYBOARD;
      _myInputs[0].U.ki.wVk = vks;
      _myInputs[0].U.ki.wScan = scanCode;
      ExternMethods.SendInput(1, _myInputs, INPUT.Size);

      Thread.Sleep(duration);
      _myInputs[0].U.ki.dwFlags = _myInputs[0].U.ki.dwFlags | KEYEVENTF.KEYUP; // Key up.
      ExternMethods.SendInput(1, _myInputs, INPUT.Size);
    }

    /// <summary>
    /// This method is a bit of a hack. From what I've read,
    /// there seems to be no easy way in the Windows API to
    /// determine if a scan code is considered an extended
    /// key or not. For my simple usage here, I tested
    /// pressing all keys on my keyboard and observed the
    /// results. This is not guaranteed to be reliable for
    /// others, or indeed future-proof. However, in
    /// the context of this small home project, it seems
    /// adequate.
    /// </summary>
    /// <param name="scanCode">A scan code to look up to determine if it's an extended key or not.</param>
    /// <returns>True if the scan code represents an extended key, false otherwise</returns>
    private bool IsExtendedKey(ScanCodeShort scanCode)
    {
      bool result = false;

      switch (scanCode)
      {
        case ScanCodeShort.INSERT:
        case ScanCodeShort.DELETE:
        case ScanCodeShort.HOME:
        case ScanCodeShort.END:
        case ScanCodeShort.PRIOR:
        case ScanCodeShort.NEXT:
        case ScanCodeShort.NUMLOCK:
        case ScanCodeShort.DIVIDE:
        case ScanCodeShort.RETURN:
        case ScanCodeShort.LWIN:
        case ScanCodeShort.RWIN:
        case ScanCodeShort.MENU:
        case ScanCodeShort.APPS:
        case ScanCodeShort.RCONTROL:
        case ScanCodeShort.LEFT:
        case ScanCodeShort.UP:
        case ScanCodeShort.RIGHT:
        case ScanCodeShort.DOWN:
          result = true;
          break;
        default:
          break;
      }
      return result;
    }

    private void openToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
      {
        string fileData = File.ReadAllText(openFileDialog1.FileName);
        this.MovesTextBox.Clear();
        this.MovesTextBox.Text = fileData;
        this.MovesTextBox.Focus();
        this.MovesTextBox.SelectionStart = 0;
        this.MovesTextBox.SelectionLength = 0;
      }
    }

    private void undoToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.MovesTextBox.Undo();
    }

    private void redoToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    private void cutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.MovesTextBox.Cut();
    }

    private void copyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.MovesTextBox.Copy();
    }

    private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.MovesTextBox.Paste();
    }

    private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.MovesTextBox.SelectAll();
    }

  }
}
