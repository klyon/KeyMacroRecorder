﻿using System.Collections.Generic;
using System;

namespace KeyMacroRecorder
{
  internal class Command
  {
    public VirtualKeyShort? VirtualKeyShort { get; set; }
    public int Duration { get; set; }
  }

  internal class Parser
  {
    public static void Parse(string commandList, List<Command> result)
    {
      var lines = commandList.Split(new string[] { "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);

      foreach (var line in lines)
      {
        char commandCode = char.ToUpper(line[0]);

        if (commandCode == '/' || commandCode == '-')
          continue; // Skip this line. It's a comment.

        Command c = new Command();

        if (commandCode == 'L')
          c.VirtualKeyShort = VirtualKeyShort.LEFT;
        else if (commandCode == 'R')
          c.VirtualKeyShort = VirtualKeyShort.RIGHT;
        else if (commandCode == 'F')
          c.VirtualKeyShort = VirtualKeyShort.UP;
        else if (commandCode == '\\')
          c.VirtualKeyShort = VirtualKeyShort.RETURN;
        else if (commandCode == '^')
          c.VirtualKeyShort = VirtualKeyShort.SPACE;
        else if (commandCode == 'P')
          c.VirtualKeyShort = null;
        else
          throw new Exception("Unsupported CommandCode: " + commandCode);

        int duration;

        if (!int.TryParse(line.Substring(1), out duration))
          duration = 50; // Default duration if none specified.

        c.Duration = duration;

        result.Add(c);
      }
    }

    private static void AddHardCodedMoves(List<Command> result)
    {
      // TODO: Execute moves.

      // Getting in position.
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 2000 });
      result.Add(new Command() { VirtualKeyShort = null, Duration = 2000 });

      // Through "Doing Things The Hard Way"
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.UP, Duration = 50 });

      result.Add(new Command() { VirtualKeyShort = null, Duration = 600 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 500 });

      result.Add(new Command() { VirtualKeyShort = null, Duration = 300 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.LEFT, Duration = 570 });

      // As far as EasyModeUnlocked

      result.Add(new Command() { VirtualKeyShort = null, Duration = 1450 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 250 });

      result.Add(new Command() { VirtualKeyShort = null, Duration = 500 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 250 });


      result.Add(new Command() { VirtualKeyShort = null, Duration = 400 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.LEFT, Duration = 300 });

      // TODO: This should be halpf way - flip now!
      result.Add(new Command() { VirtualKeyShort = null, Duration = 130 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.UP, Duration = 50 });

      // Coming back down!
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.LEFT, Duration = 250 });
      result.Add(new Command() { VirtualKeyShort = null, Duration = 260 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 375 });


      result.Add(new Command() { VirtualKeyShort = null, Duration = 350 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.LEFT, Duration = 200 });

      result.Add(new Command() { VirtualKeyShort = null, Duration = 1450 });
      result.Add(new Command() { VirtualKeyShort = VirtualKeyShort.RIGHT, Duration = 500 });

    }

    private static void AddFlip(List<Command> commands)
    {
      commands.Add(new Command() { VirtualKeyShort = VirtualKeyShort.UP, Duration = 50 });
    }

  }
}
